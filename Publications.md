---
layout: page
title: Publications
---

<h1 class="page-title">Publications</h1>

<div style="height:30px;">&nbsp;</div>

<h3 class="page-title">Peer-reviewed journal articles</h3>

<div style="height:15px;">&nbsp;</div>

0. Gastauer M, Nascimento Jr WR, Frois Caldeira C, Ramos SJ, Souza-Filho PWM & Féret J-B (2022)
[Spectral diversity allows remote detection of the rehabilitation status in an Amazonian iron mining complex](https://doi.org/10.1016/j.jag.2021.102653){:target="_blank"}
*International Journal of Applied Earth Observation and Geoinformation* 106, 102653

0. De Almeida DRA, Broadbent EN, Ferreira MP, Meli P, Zambrano AMA, Gorgens EB, de Resende AF, de Almeida CT, Do Amaral CH, Dalla Corte AP, Silva CA, Romanelli JP, Prata GA, de Almeida Papa D, Stark SC, Valbuena R, Nelson BW, Guillemot J, Féret J-B, Chazdon R & Brancalion PHS (2021)
[Monitoring restored tropical forest diversity and structure through UAV-borne hyperspectral and lidar fusion](https://doi.org/10.1016/j.rse.2021.112582){:target="_blank"}
*Remote Sensing of Environment* 262, 1, 112505

0. Hauser LT , Féret J-B, Binh NA, van der Windt N, Sil ÂF, Timmermans J, Soudzilovskaia NA & van Bodegom PM (2021)
[Towards scalable estimation of plant functional diversity from Sentinel-2: In-situ validation in a heterogeneous (semi-) natural landscape](https://doi.org/10.1016/j.rse.2021.112505){:target="_blank"}
*Remote Sensing of Environment* 264, 1, 112582

0. Torresani M, Feilhauer H, Rocchini D, Féret J-B, Zebisch M & Tonon G (2021)
[Which optical traits enable an estimation of tree species diversity based on the Spectral Variation Hypothesis?]( https://doi.org/10.1111/avsc.12586){:target="_blank"}
*Applied Vegetation Science* 24, 2.

0. Ebengo DM, de Boissieu F, Vincent G, Weber C & Féret J-B (2021)
[Simulating Imaging Spectroscopy in Tropical Forest with 3D Radiative Transfer Modeling](https://www.mdpi.com/2072-4292/13/11/2120){:target="_blank"}
*Remote Sensing* 13, 2120

0. Chraibi E, Arnold H, Luque S, Deacon A, Magurran AE & Féret J-B (2021)
[A Remote Sensing Approach to Understanding Patterns of Secondary Succession in Tropical Forest](https://www.mdpi.com/2072-4292/13/11/2148){:target="_blank"}
*Remote Sensing* 13, 2148

0. Rocchini D, Salvatori N, Beierkuhnlein C, Chiarucci A, de Boissieu F, Foerster M, Garzon-Lopez CX, Gillespie TW, Hauffe HC, He KS, Kleinschmit B, Lenoir J, Malavasi M, Moudrý V, Nagendra H, Payne D, Šímová P, Torresani M, Wegmann M & Féret J-B (2021)
[From local spectral species to global spectral communities: a benchmark for ecosystem diversity estimate by remote sensing.](https://doi.org/10.1016/j.ecoinf.2020.101195){:target="_blank"}
*Ecological Informatics* 61, 101195.

0. Spafford L, le Maire G, MacDougall A, de Boissieu F & Féret J-B (2021)
[Spectral subdomains and prior estimation of leaf structure improves PROSPECT inversion on reflectance or transmittance alone](https://doi.org/10.1016/j.rse.2020.112176){:target="_blank"}
*Remote Sensing of Environment* 252, 112176.

0. Féret J-B, Berger K, de Boissieu F & Malenovský Z (2021)
[PROSPECT-PRO for estimating content of nitrogen-containing leaf proteins and other carbon-based constituents](https://doi.org/10.1016/j.rse.2020.112173){:target="_blank"}
*Remote Sensing of Environment* 252, 112173.

0. Berger K, Verrelst J, Féret J-B, Hank T, Wocher M, Mauser W, Camps-Valls G (2020)
[Retrieval of aboveground crop nitrogen content with a hybrid machine learning method.](https://doi.org/10.1016/j.jag.2020.102174){:target="_blank"}
*International Journal of Applied Earth Observation and Geoinformation* 92, 102174.

0. Laybros A, Aubry-Kientz M, Féret J-B, Bedeau C, Brunaux O, Derroire G, Vincent G (2020)
[Quantitative Airborne Inventories in Dense Tropical Forest Using Imaging Spectroscopy.](https://www.mdpi.com/2072-4292/12/10/1577){:target="_blank"}
*Remote Sensing* 12, 1577.

0. Berger K, Verrelst J, Féret J-B, Wang Z, Wocher M, Strathmann M, Danner M, Mauser W & Hank T (2020)
[Crop nitrogen monitoring: Recent progress and principal developments in the context of imaging spectroscopy missions.](https://www.sciencedirect.com/science/article/abs/pii/S0034425720301280){:target="_blank"}
*Remote Sensing of Environment* 242, 111758.

0. Féret J-B & de Boissieu F (2020)
[biodivMapR: An r package for α- and β-diversity mapping using remotely sensed images.](https://besjournals.onlinelibrary.wiley.com/doi/full/10.1111/2041-210X.13310){:target="_blank"}
*Methods in Ecology and Evolution* 11, 64-70.

0. Féret J-B, le Maire G, Jay S, Berveiller D, Bendoula R, Hmimina G, Cheraiet A, Oliveira JC, Ponzoni FJ, Solanki T, de Boissieu F, Chave J, Nouvellon Y, Porcar-Castell A, Proisy C, Soudani K, Gastellu-Etchegorry J-P & Lefèvre-Fonollosa M-J (2019)
[Estimating leaf mass per area and equivalent water thickness based on leaf optical properties: Potential and limitations of physical modeling and machine learning.](https://www.sciencedirect.com/science/article/pii/S0034425718305030){:target="_blank"}
*Remote Sensing of Environment* 231, 110959.

0. Réjou-Méchain M, Barbier N, Couteron P, Ploton P, Vincent G, Herold M, Mermoz S, Saatchi S, Chave J, de Boissieu F, Féret J-B, Momo Takoudjou S & Pélissier R (2019)
[Upscaling Forest biomass from field to satellite measurements: sources of errors and ways to reduce them.](https://link.springer.com/article/10.1007/s10712-019-09532-0){:target="_blank"}
*Surveys in Geophysics* 40(4), 881-911.

0. Laybros A, Schläpfer D, Féret J-B, Descroix L, Bedeau C, Lefevre M-J & Vincent G (2019)
[Across Date Species Detection Using Airborne Imaging Spectroscopy.](https://www.mdpi.com/2072-4292/11/7/789){:target="_blank"}
*Remote Sensing* 11, 789.

0. Lang M, Alleaume S, Luque S, Baghdadi N & Féret J-B (2019)
[Complementarity between Textural and Radiometric Indices From Airborne and Spaceborne Multi VHSR Data: Disentangling the Complexity of Heterogeneous Landscape Matrix.](https://www.mdpi.com/2072-4292/11/6/693){:target="_blank"}
*Remote Sensing* 11, 693.

0. Gomez C, Dharumarajan S, Féret J-B, Lagacherie P, Ruiz L & Sekhar M (2019)
[Use of sentinel-2 time-series images for classification and uncertainty analysis of inherent biophysical property: Case of soil texture mapping.](https://www.mdpi.com/2072-4292/11/5/565){:target="_blank"}
*Remote Sensing* 11, 565.

0. Morel J, Jay S, Féret J-B, Bakache A, Bendoula R, Carreel F & Gorretta N (2018)
[Exploring the potential of PROCOSINE and close-range hyperspectral imaging to study the effects of fungal diseases on leaf physiology.](https://www.nature.com/articles/s41598-018-34429-0){:target="_blank"}
*Nature Scientific reports* 8, 15933.

0. Alleaume S, Dusseux P, Thierion V, Commagnac L, Laventure S, Lang M, Féret J-B, Hubert-Moy L & Luque S (2018)
[A generic remote sensing approach to derive operational essential biodiversity variables (EBVs) for conservation planning.](https://onlinelibrary.wiley.com/doi/pdf/10.1111/2041-210X.13033){:target="_blank"}
*Methods in Ecology and Evolution* 9, 1822-1836.

0. Rocchini D, Luque S, Pettorelli N, Bastin L, Doktor D, Faedi N, Feilhauer H, Féret J-B, Foody GM, Gavish Y, Godinho S, Kunin WE, Lausch A, Leitão PJ, Marcantonio M, Neteler M, Ricotta C, Schmidtlein S, Vihervaara P, Wegmann M & Nagendra H (2018)
[Measuring β-diversity by remote sensing: A challenge for biodiversity monitoring.](https://besjournals.onlinelibrary.wiley.com/doi/full/10.1111/2041-210X.12941){:target="_blank"}
*Methods in Ecology and Evolution* 9, 1787-1798.

0. Ferreira MP, Féret J-B, Grau E, Gastellu-Etchegorry J-P, do Amaral CH, Shimabukuro YE, de Souza Filho CR (2018)
[Retrieving structural and chemical properties of individual tree crowns in a highly diverse tropical forest with 3D radiative transfer modeling and imaging spectroscopy.](https://www.sciencedirect.com/science/article/abs/pii/S0034425718301780){:target="_blank"}
*Remote Sensing of Environment* 211, 276-291.

0. Lang M, Alleaume S, Luque S, Baghdadi N & Féret J-B (2018)
[Monitoring and characterizing heterogeneous Mediterranean landscapes with continuous textural indices based on VHSR imagery.](https://www.mdpi.com/2072-4292/10/6/868){:target="_blank"}
*Remote Sensing* 10, 868.

0. Féret J-B, Noble SD, Gitelson AA & Jacquemoud S (2017)
[PROSPECT-Dynamic: towards modeling leaf optical properties through a complete lifecycle.](https://www.sciencedirect.com/science/article/pii/S0034425717300962){:target="_blank"}
*Remote Sensing of Environment* 193, 204-215.

0. Albetis J, Duthoit S, Guttler F, Jacquin A, Goulard M, Poilvé H, Féret J-B & Dedieu G (2017)
[Detection of the Flavescence dorée} grapevine disease by UAV multispectral imagery.](https://www.mdpi.com/2072-4292/9/4/308){:target="_blank"}
*Remote Sensing* 9, 308-327.

0. de Castro Oliveira J, Féret J-B, Ponzoni FG, Nouvellon Y, Gastellu-Etchegorry J-P, Campoe OC, Stape JL, Estraviz Rodriguez LC & le Maire G. (2017)
[Simulating the canopy reflectance of different Eucalypt genotypes with the DART 3D model.](https://hal.archives-ouvertes.fr/hal-01772292/document){:target="_blank"}
*IEEE JSTARS* 10, 4844-4852.

0. Olagoke A, Proisy C, Féret J-B, Blanchard E, Fromard F, Mehlig U, de Menezes MM, dos Santos VF & Berger U (2016)
[Extended biomass allometric equations for large mangrove trees from terrestrial LiDAR data.](https://link.springer.com/article/10.1007/s00468-015-1334-9){:target="_blank"}
*Trees* 30, 935-947.

0. Rocchini D, Boyd DS, Féret J-B, Foody GM, He KS, Lausch A, Nagendra H, Wegmann M & Pettorelli N (2016)
[Satellite remote sensing to monitor species diversity: potential and pitfalls.](https://zslpublications.onlinelibrary.wiley.com/doi/pdf/10.1002/rse2.9){:target="_blank"}
*Remote Sensing in Ecology and Conservation* 2, 25-36.

0. Viennois G, Proisy C, Rahmania R, Andayani A, Subki B, Suhardjono, Féret J-B, Prosperi J, Sidik F, Widagti N, Germain O & Gaspar P (2015)
[Multitemporal Analysis of High-Spatial-Resolution Optical Satellite Imagery for Mangrove Species Mapping in Bali, Indonesia.](https://ieeexplore.ieee.org/abstract/document/7464810){:target="_blank"}
*IEEE JSTARS* 8(5), 2294-2305.

0. Féret J-B, Corbane C & Alleaume S (2015)
[Detecting the Phenology and Discriminating Mediterranean Natural Habitats With Multispectral Sensors—An Analysis Based on Multiseasonal Field Spectra.](https://ieeexplore.ieee.org/abstract/document/7118634?section=abstract){:target="_blank"}
*IEEE JSTARS* 8(5), 2294-2305.

0. Tochon G, Féret J-B, Valero S, Martin RE, Knapp DE, Salembier P, Chanussot J & Asner GP (2015)
[On the use of binary partition trees for the tree crown segmentation of tropical rainforest hyperspectral images.](https://www.sciencedirect.com/science/article/pii/S0034425714005124){:target="_blank"}
*Remote Sensing of Environment* 159, 31-331.

0. Gastellu-Etchegorry J-P, Yin T, Lauret N, Cajgfinger T, Gregoire T, Grau E, Féret J-B, Lopes M, Guilleux J, Dedieu G, Malenovsky Z, Cook BD, Morton D, Rubio J, Durrieu S, Cazanave G, Martin E. & Ristorcelli T (2015)
[Discrete anisotropic radiative transfer (DART 5) for modelling airborne and satellite spectroradiometer and LIDAR acquisitions of natural and urban landscapes.](https://www.mdpi.com/2072-4292/7/2/1667/htm){:target="_blank"}
*Remote Sensing* 7, 1667-1701.

0. Féret J-B & Asner GP (2014)
[Microtopographic Controls on Lowland Amazonian Canopy Diversity from Imaging Spectroscopy.](https://esajournals.onlinelibrary.wiley.com/doi/pdf/10.1890/13-1896.1){:target="_blank"}
*Ecological Applications* 24, 1297-1310.

0. Féret J-B & Asner GP (2014)
[Mapping tropical forest canopy diversity using high-fidelity imaging spectroscopy.](https://esajournals.onlinelibrary.wiley.com/doi/full/10.1890/13-1824.1){:target="_blank"}
*Ecological Applications* 24, 1289-1296.

0. Cheng T, Rivard B, Sanchez-Azofeifa AG, Féret J-B, Jacquemoud S & Ustin SL (2014)
[Deriving leaf mass per area (LMA) from foliar reflectance across a variety of plant species using continuous wavelet analysis.](https://www.sciencedirect.com/science/article/pii/S0924271613002311){:target="_blank"}
*ISPRS Journal of Photogrammetry and Remote Sensing* 87, 28-38.

0. Baldeck CA, Colgan MS, Féret J-B, Levick SR, Martin RE & Asner GP (2014)
[Landscape-scale variation in plant community composition of an African savanna from airborne species mapping.](https://esajournals.onlinelibrary.wiley.com/doi/pdf/10.1890/13-0307.1){:target="_blank"}
*Ecological Applications* 24, 84-93.

0. Tochon G, Féret J-B, Valero S, Martin RE, Tupayachi R, Chanussot J, Salembier P & Asner GP (2013)
[Segmentation hyperspectrale de forêts tropicales par arbres de partition binaires.](https://imatge.upc.edu/web/sites/default/files/pub/aTochon13.pdf{:target="_blank"}
*Revue Française de Photogrammétrie et de Télédétection*

0. Féret J-B & Asner GP (2013)
[Tree species discrimination in tropical forests using airborne imaging spectroscopy.](https://ieeexplore.ieee.org/stamp/stamp.jsp?arnumber=6241414){:target="_blank"}
*IEEE Transactions on Geoscience and Remote Sensing* 51(1), 73-84.

0. Colgan MS, Baldeck CA, Féret J-B & Asner GP (2012)
[Mapping savanna tree species at ecosystem scales using support vector machine classification and BRDF correction on airborne hyperspectral and LiDAR data.](https://www.mdpi.com/2072-4292/4/11/3462){:target="_blank"}
*Remote Sensing* 4(11), 3462-3480.

0. Féret J-B & Asner GP (2012)
[Semi-supervised methods to identify individual crowns of lowland tropical canopy species using imaging spectroscopy and LiDAR.](https://www.mdpi.com/2072-4292/4/8/2457){:target="_blank"}
*Remote Sensing* 4(8), 2457-2476.

0. Cheng T, Rivard B, Sanchez-Azofeifa AG, Féret J-B, Jacquemoud S & Ustin SL(2012)
[Predicting leaf gravimetric water content from foliar reflectance across a range of plant species using continuous wavelet analysis.](https://www.sciencedirect.com/science/article/pii/S0176161712001794){:target="_blank"}
*Journal of Plant Physiology}* 169(12), 1134-1142.

0. Féret J-B, François C, Gitelson A, Asner GP, Barry KM, Panigada C, Richardson AD & Jacquemoud S (2011)
[Optimizing spectral indices and chemometric analysis of leaf chemical properties using radiative transfer modeling.](https://www.sciencedirect.com/science/article/pii/S0034425711002409){:target="_blank"}
*Remote Sensing of Environment* 115, 2742-2750.

0. Féret J-B & Asner GP (2011)
[Spectroscopic classification of tropical forest species using radiative transfer modeling.](https://www.sciencedirect.com/science/article/pii/S0034425711001763){:target="_blank"}
*Remote Sensing of Environment* 115, 2415-2422.

0. Deroussi S, Diament M, Féret J-B, Nebut T & Staudacher T (2009)
[Localization of cavities in a thick lava flow by microgravimetry.](https://www.sciencedirect.com/science/article/pii/S0377027308005428){:target="_blank"}
*Journal of Volcanology and Geothermal Research* 184, 193-198. 

0. Féret J-B, François C, Asner GP, Gitelson AA, Martin RE, Bidel LPR, Ustin SL, le Maire G & Jacquemoud S (2008)
[PROSPECT-4 and 5: Advances in the leaf optical properties model separating photosynthetic pigments.](https://www.sciencedirect.com/science/article/pii/S0034425708000813){:target="_blank"}
*Remote Sensing of Environment* 112(6),3030-3043.

0. Lagacherie P, Baret F, Féret J-B, Madeira Netto J & Robbez-Masson J-M (2008)
[Estimation of soil clay and calcium carbonate using laboratory, field and airborne hyperspectral measurements.](https://www.sciencedirect.com/science/article/pii/S003442570700291X){:target="_blank"}
*Remote Sensing of Environment* 112(3), 825-835.
{: reversed="reversed"}

<div style="height:30px;">&nbsp;</div>

<h3 class="page-title">Submitted papers</h3>

<div style="height:15px;">&nbsp;</div>




<!---

* Mazzoli M, Molas A, Bassolas A, Lenormand M, Colet P & Ramasco JJ (2019)
[Field Theory for recurrent mobility.](https://arxiv.org/pdf/1803.05275.pdf){:target="_blank"}
*arXiv preprint* arXiv:1803.05275.
[[arXiv](https://arxiv.org/abs/1803.05275){:target="_blank"}]

-->

<div style="height:30px;">&nbsp;</div>

<h3 class="page-title">Conference papers</h3>

<div style="height:15px;">&nbsp;</div>

{: reversed="reversed"}

<div style="height:30px;">&nbsp;</div>

<h3 class="page-title">Book chapters</h3>

<div style="height:15px;">&nbsp;</div>

* Bajjouk T, de Boissieu F, Chanussot J, Douté S, Dumont M, Féret J-B, Masson T, Minghelli A, Mouquet P, Schmidt F, Dalla Mura M (2020)
[Applications in remote sensing—natural landscapes.](https://www.sciencedirect.com/science/article/pii/B978044463977600016X){:target="_blank"}
*Chapter 3.1 in Data Handling in Science and Technology - Volume 32: Hyperspectral Imaging*, Amigo JM (Eds.), Elsevier Amsterdam, 371-410.

* Tusa E, Laybros A, Monnet J-M, Dalla Mura M, Barré J-B, Vincent G, Dalponte M, Féret J-B, Chanussot J (2020)
[Fusion of hyperspectral imaging and LiDAR for forest monitoring.](https://www.sciencedirect.com/science/article/pii/B9780444639776000134){:target="_blank"}
*Chapter 2.11 in Data Handling in Science and Technology - Volume 32: Hyperspectral Imaging*, Amigo JM (Eds.), Elsevier Amsterdam, 281-303.

* Féret J-B, Rocchini D, He K, Nagendra H, Luque S (2017)
[Forest species mapping.](http://www.gofcgold.wur.nl/documents/BiodiversitySourcebook/BiodiversitySourcebook.pdf){:target="_blank"}
*in A Sourcebook of Methods and Procedures for Monitoring Essential Biodiversity Variables in Tropical Forests with Remote Sensing*, GOFC-GOLD, 164-181.

* Proisy C, Féret J-B, Lauret N, Gastellu-Etchegorry JP (2016)
[Mangrove Forest Dynamics Using Very High Spatial Resolution Optical Remote Sensing.](https://www.sciencedirect.com/science/article/pii/B9781785481604500078){:target="_blank"}
*in Land Surface Remote Sensing in Urban and Coastal Areas*, Baghdadi N & Zribi M (Eds.), Elsevier , 269-295.

<div style="height:30px;">&nbsp;</div>

<h3 class="page-title">Theses</h3>

<div style="height:15px;">&nbsp;</div>

<!---

* Lenormand M (2020) 
[Initialize and calibrate a dynamic stochastic microsimulation model: Application to the *SimVillages* model.](http://tel.archives-ouvertes.fr/docs/00/76/49/29/PDF/Lenormand-M-2012.pdf){:target="_blank"}
*HDR Thesis*, University of Montpellier, Montpellier, France.

-->

* Féret J-B (2009) 
[Apport de la modélisation pour l'estimation de la teneur en pigments foliaires par télédétection.](http://www.ipgp.fr/~jacquemoud/publications/these_Feret_2009.pdf){:target="_blank"}
*PhD Thesis*, Pierre et Marie Curie University, Paris 6, France.

<div style="height:40px;">&nbsp;</div>
