---
layout: page
title: Projects
---

<h1 class="page-title">Research projects</h1>

<div style="height:30px;">&nbsp;</div>

<h3 class="page-title">On-going</h3>

<div style="height:15px;">&nbsp;</div>

[**BioCop**](https://jbferet.github.io/biodivMapR/index.html){:target="_blank"} (Tropical BIOdiversity monitoring with COPernicus Sentinel-2 satellites) is a Young Researchers project funded by the French National Research Agency. The main objective of the BioCop project is to study the potential of Sentinel-2 data for the estimation of a selection of SRS-EBV candidates in the context of tropical forests. [[Role: Scientific Coordinator](https://jbferet.github.io/biodivMapR/index.html){:target="_blank"}]

<div style="height:10px;">&nbsp;</div>

<center>
  <img src="{{ '/assets/img/biodiversite-montage.png' | relative_url }}" border="0" height="200">
</center>

<div style="height:15px;">&nbsp;</div>

[**PROSPECT**](https://jbferet.gitlab.io/prospect/){:target="_blank"}, the most widespread leaf model in the [**remote sensing community**](http://opticleaf.ipgp.fr/index.php?page=models){:target="_blank"}, was initially developed by [**Jacquemoud & Baret (1990)**](https://www.sciencedirect.com/science/article/pii/003442579090100Z#aep-article-footnote-id1){:target="_blank"}. I started working on PROSPECT during my PhD, and developed the [**latest official versions**](http://teledetection.ipgp.jussieu.fr/prosail/){:target="_blank"} since then. [[Role: Scientific Coordinator](https://jbferet.gitlab.io/prospect/){:target="_blank"}].

<div style="height:10px;">&nbsp;</div>

<center>
  <img src="{{ '/assets/img/PROSPECT_LOGO.png' | relative_url }}" border="0" height="120">
</center>

<div style="height:30px;">&nbsp;</div>

[**HyperBio (Estimating biodiversity using hyperspectral images from satellite: preparation of future spacebone imaging spectroscopy sensors)**] is a CNES/TOSCA project. [Role: Principal Investigator]

<div style="height:30px;">&nbsp;</div>

[**HyperTropik (Estimating tropical biodiversity using hyperspectral images from satellite: preparation of future spacebone imaging spectroscopy sensors)**] is a CNES/TOSCA project. [Role: Principal Investigator]

<div style="height:30px;">&nbsp;</div>

