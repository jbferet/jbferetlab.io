---
layout: page
title: Codes
---

<h1 class="page-title">Codes</h1>

<div style="height:10px;">&nbsp;</div>

Some codes that I wrote or contributed to write are accessible from [my GitHub account](https://github.com/jbferet/){:target="_blank"} or [my Gitlab account](https://gitlab.com/jbferet/){:target="_blank"}.

<div style="height:10px;">&nbsp;</div>

<h3 class="page-title">R packages</h3>

<div style="height:10px;">&nbsp;</div>

[**biodivMapR**](https://jbferet.github.io/biodivMapR/index.html){:target="_blank"}: an R package for α- and β-diversity mapping using remotely-sensed images

<div style="height:10px;">&nbsp;</div>

[**prosail**](https://jbferet.gitlab.io/prosail/){:target="_blank"}: an R package for the simulation of canopy reflectance using the model PROSAIL (PROSPECT+SAIL). Includes routines to perform hybrid inversion on optical images, spectral index computation and sensor simulator. 

<div style="height:10px;">&nbsp;</div>

[**prospect**](https://jbferet.gitlab.io/prospect/){:target="_blank"}: an R package including the latest developments related to the leaf model PROSPECT. It can be used in forward mode for the simulation of leaf optical properties using the PROSPECT model, and in inverse mode for the estimation of leaf chemical properties based on leaf optical properties, using iterative optimization.

<div style="height:10px;">&nbsp;</div>

[**preprocS2**](https://jbferet.gitlab.io/preprocs2/){:target="_blank"}: A R package dedicated to basic preprocessing of Sentinel-2 Level-2A reflectance images. Includes functions to perform atmospheric corrections with sen2cor, stack and crop S2 images in SAFE format, THEIA format and LaSRC format. 

<div style="height:10px;">&nbsp;</div>

<h3 class="page-title">python packages</h3>

<div style="height:10px;">&nbsp;</div>

[**pytools4dart**](https://gitlab.com/pytools4dart/pytools4dart){:target="_blank"}: Python API for DART simulator. 99% of this package was developed by [**Florian de Boissieu**](https://gitlab.com/floriandeboissieu){:target="_blank"}. 

<div style="height:10px;">&nbsp;</div>

[**fordead**](https://fordead.gitlab.io/fordead_package/){:target="_blank"}: Python package for monitoring of FORest DEgradation And Dieback based on Sentinel-2 time series analysis. Developed by **Raphael Dutrieux** and [**Florian de Boissieu**](https://gitlab.com/floriandeboissieu){:target="_blank"}. 

<div style="height:10px;">&nbsp;</div>

<h3 class="page-title">other codes</h3>

<div style="height:10px;">&nbsp;</div>

[**prospect (matlab)**](https://gitlab.com/jbferet/prospect_pro_matlab){:target="_blank"}: Matlab distribution of the latest developments related to the leaf model PROSPECT. the version is PROSPECT-PRO. No associated inversion procedures provided.
