---
layout: page
title: Misc
---

<h1 class="page-title">Students, teaching and committing</h1>

<div style="height:30px;">&nbsp;</div>

<h3 class="page-title"> People </h3>

<h5 class="page-title"> <span style="color:gray;font-weight: bold;">PhD students</span> </h5>
* Colette Badourdine(2019-2022) 
* Eric Chraibi(2018-2021) 
* [Dav Ebengo](https://www.researchgate.net/profile/Dav_Ebengo){:target="_blank"} (2017-2020) 
* [Marc Lang](https://www.researchgate.net/profile/Marc_Lang2){:target="_blank"} (2015-2019) 

<h5 class="page-title"> <span style="color:gray;font-weight: bold;">Undergraduate and Master's students</span> </h5>
* Eric Chraibi (2018)
* [Yousra Hamrouni](https://www.researchgate.net/profile/Yousra_Hamrouni){:target="_blank"} (2015)
* [Guillaume Tochon](https://www.researchgate.net/profile/Guillaume_Tochon){:target="_blank"} (2011)

<h6 class="page-title"> <span style="color:gray;font-weight: bold;">Visiting scholars</span> </h6>
* [Matheus Pinheiro Ferreira](https://www.researchgate.net/profile/Matheus_Ferreira2){:target="_blank"} - Instituto Militar de Engenharia (IME) (June 2015)


<h3 class="page-title"> Organization of scientific events</h3>
* [6e colloque scientifique du groupe SFPT-GH](https://sfpt-gh-2018.sciencesconf.org/){:target="_blank"} - 6th scientific workshop for the French hyperspectral coomunity, France (17-18 May 2018)

<div style="height:20px;">&nbsp;</div>

<h3 class="page-title"> Teaching</h3>

<div style="height:20px;">&nbsp;</div>
* Remote sensing of vegetation & monitoring of natural ecosystems - MSc in Geomatic - AgroParisTech Montpellier (since 2015)
* Mapping biodiversity wih remotely-sensed information - [Mastère SILAT](http://www2.agroparistech.fr/-MS-SILAT-Systemes-d-informations-localisees-pour-l-amenagement-des-territoires-.html) - AgroParisTech Montpellier (since 2015)

<h3 class="page-title"> Committee involvement </h3>
* PhD steering commitee - [Eduardo Alejandro Tusa Jumbo](https://www.researchgate.net/profile/Eduardo_Tusa){:target="_blank"} - [GIPSA lab](http://www.gipsa-lab.fr/){:target="_blank"} (2017 - present)
* PhD steering commitee - [Yousra Hamrouni](https://www.researchgate.net/profile/Yousra_Hamrouni){:target="_blank"} - [Dynafor Research Unit](https://www.dynafor.fr/){:target="_blank"} (2017 - present)
* PhD steering commitee - [Anthony Laybros](https://www.researchgate.net/profile/Anthony_Laybros2){:target="_blank"} - [AMAP Research Unit](http://amap.cirad.fr/en/index.php){:target="_blank"} (2018 - present) 

<div style="height:20px;">&nbsp;</div>

<h3 class="page-title"> Reviewer for</h3>

* [Remote Sensing of Environment](https://www.journals.elsevier.com/remote-sensing-of-environment){:target="_blank"}
* [Remote Sensing](https://www.mdpi.com/journal/remotesensing){:target="_blank"}
* [IEEE Transactions on Geoscience and Remote Sensing](https://ieeexplore.ieee.org/xpl/RecentIssue.jsp?punumber=36){:target="_blank"}
* [IEEE Journal of Selected Topics in Applied Earth Observations and Remote Sensing](https://ieeexplore.ieee.org/xpl/RecentIssue.jsp?punumber=4609443){:target="_blank"}
* [Ecology Letters](https://onlinelibrary.wiley.com/journal/14610248){:target="_blank"}
* [International Journal of Remote Sensing](https://www.tandfonline.com/toc/tres20/current){:target="_blank"}
* [Journal of Vegetation Science](https://onlinelibrary.wiley.com/journal/16541103){:target="_blank"}
* [Remote Sensing in Ecology and Conservation](https://zslpublications.onlinelibrary.wiley.com/journal/20563485){:target="_blank"}
* [Field Crops Research](https://www.journals.elsevier.com/field-crops-research){:target="_blank"}

