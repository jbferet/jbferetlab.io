---
layout: page
title: Archive
---

<h1 class="page-title">Blog Posts</h1>

<div style="height:30px;">&nbsp;</div>

{% for post in site.posts %}
  [ {{ post.title }} ]({{ post.url }})
  <!-- * **{{ post.date | date_to_string }}:** [ {{ post.title }} ]({{ post.url }}) -->
{% endfor %}