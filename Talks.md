---
layout: page
title: Talks
---

<h1 class="page-title">Talks</h1>

<div style="height:30px;">&nbsp;</div>

<h3 class="page-title">2019</h3>
* [Linking remote sensing with physical modeling of vegetation: Methods, challenges and perspectives](http://ecpa2019.agrotic.org/keynote-speakers/#jb){:target="_blank"}  &nbsp;
* [Alpha and Beta Diversity Mapping in Tropical Forests Using Sentinel-2 Imagery](https://lps19.esa.int/NikalWebsitePortal/living-planet-symposium-2019/lps19/Speaker){:target="_blank"}  &nbsp;
* [Cartographie de la biodiversité forestière à l’aide d’imagerie multi & hyperspectrale](https://www.youtube.com/watch?v=6fJ0IsF3tyM&t=1364s){:target="_blank"}  &nbsp;
* [Modeling leaf optical properties for the remotely sensed estimation of leaf traits and beyond](http://www.ieee-whispers.com/wp-content/uploads/2018/07/Whispers_Program_2018_v1.0.pdf){:target="_blank"}  &nbsp;

<h3 class="page-title">2018</h3>
* [Monitoring natural ecosystems using multi / hyperspectral imaging and physical modeling] &nbsp;
* [S2 BOA reflectance in the context of tropical rainforest: preliminary illustration of the challenges for the extraction of vegetation properties](https://theia.sedoo.fr/wp-content-theia/uploads/sites/2/2019/05/52-Maja2018-Feret_leMaire.pdf){:target="_blank"}  &nbsp;
* [Monitoring biophysical properties of Eucalyptus plantations using Sentinel 2 time series](https://theia.sedoo.fr/wp-content-theia/uploads/sites/2/2019/05/52-Maja2018-Feret_leMaire.pdf){:target="_blank"}  &nbsp;

<h3 class="page-title">2017</h3>
* [Contributions of radiative transfer models to map ecosystem functioning](http://remote-sensing-biodiversity.org/workshop-on-remote-sensing-of-ecosystem-functioning/){:target="_blank"}  &nbsp;
* [Estimation de la biodiversité des forêts tropicales par imagerie hyperspectrale : Avancées du projet HyperTropik](https://www.sfpt.fr/hyperspectral/wp-content/uploads/2018/01/Feret_SFPT_GH_2017.pdf){:target="_blank"}  &nbsp;

