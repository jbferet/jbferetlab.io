---
layout: page
title: Resume
---

<h1 class="page-title">Resume</h1>

<div style="height:30px;">&nbsp;</div>

<h3 class="page-title">Work experiences</h3>

<span style="color:gray;font-weight: bold;">Since 2014</span> &nbsp; **Research Associate** &nbsp; [TETIS (INRAE), Montpellier, France](https://www.umr-tetis.fr/index.php/fr/){:target="_blank"}  
<span style="color:gray;font-weight: bold;">2014-2014</span> <span style="display:inline-block; width:0.07cm;"></span> &nbsp; **Postdoctoral Research Associate** &nbsp; [CESBIO, Toulouse, France](https://www.cesbio.cnrs.fr/){:target="_blank"}  
<span style="color:gray;font-weight: bold;">2010-2013</span> <span style="display:inline-block; width:0.07cm;"></span> &nbsp; **Postdoctoral Research Associate** &nbsp; [DGE, Stanford, USA](https://dge.carnegiescience.edu/){:target="_blank"}  
<span style="color:gray;font-weight: bold;">2006-2009</span> <span style="display:inline-block; width:0.07cm;"></span> &nbsp; **PhD Fellow** &nbsp; [IPGP, Paris, France](http://www.ipgp.fr/fr){:target="_blank"}  

<div style="height:30px;">&nbsp;</div>

<h3 class="page-title">Academic experiences</h3>

<span style="color:gray;font-weight: bold;">2009</span> &nbsp; **PhD in Environmental sciences** &nbsp; [Universite Pierre et Marie Curie Paris 6, Paris, France](http://www.sorbonne-universites.fr/){:target="_blank"}  
<span style="color:gray;font-weight: bold;">2005</span> &nbsp; **Engineer in agronomy** &nbsp; [Montpellier SupAgro, Montpellier, France](https://www.montpellier-supagro.fr/){:target="_blank"}  

<div style="height:30px;">&nbsp;</div>

<h3 class="page-title">Grants & Scholarships</h3>

<span style="color:gray;font-weight: bold;">2018</span> &nbsp; **ANR JCJC Biocop (4 years - 300K&#8364;)** &nbsp; [French National Research Agency](https://anr.fr/en/#){:target="_blank"}   
<span style="color:gray;font-weight: bold;">2017</span> &nbsp; **TOSCA HyperBio (3 years)** &nbsp;   
<span style="color:gray;font-weight: bold;">2015</span> &nbsp; **TOSCA HyperTropik (3 years)** &nbsp;   
